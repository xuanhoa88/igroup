/**
Question?
Feel free and contact me: baophan94@icloud.com
wWw: http://dinophan.com
**/


/*==========  CONFIG  ==========*/
var SERVER_PORT		= 2222;

/*==========  MODULES  ==========*/
var express			= require('express');
var app				= express();
var clients			= require('./app/clients.js')();

/*==========  ROUTER  ==========*/
app.set('port', SERVER_PORT);
app.get('/', function (request, response) {
	response.charset	= 'utf-8';
	response.sendFile(__dirname + '/public/index.html');
});
app.use('/', express.static(__dirname + '/public'));

/*==========  START SERVER  ==========*/
var server 			= app.listen(app.get('port'), function () {
	console.log('-- SERVER STARTED ON PORT: ' + app.get('port') + ' --');
});

var socketIO = require('socket.io').listen(server);
require('./app/handler.js')(socketIO, clients);
